# simple-webserver
this repo hosts the code used to create a webserver on a specific esp 13 arduino wifi sheild. The microcontroller used here is the piconaut, so `generic RP2040` must be selected as the board in the arduino ide, install the RP2040 support on the ide if needed.   
TX/RX should be wired to pins 12 and 13 on the piconaut, or changed in the main source file. An echo sensor should be connected on pins 10 and 11 and motor control pins should be connected on pins 14->17.   
Refer to the source code for more detail :rocket: