#include "WebServer.h"

WebServer::WebServer(Stream& atSerial, Stream& debugSerial)
  : atSerial(atSerial), debugSerial(debugSerial) {
  memset(lineBuffer, 0, sizeof(lineBuffer));
}

void WebServer::sendHttpResponse(String content) {
  atSerial.println("AT+CIPSEND=0," + String(content.length() + 90));
  delay(500); // Wait for "> "

  atSerial.print("HTTP/1.1 200 OK\r\n");
  atSerial.print("Content-Type: text/html\r\n");
  atSerial.print("Connection: close\r\n");
  atSerial.print("Content-Length: ");
  atSerial.print(content.length());
  atSerial.print("\r\n\r\n"); // End of HTTP headers
  atSerial.print(content);

  debugSerial.print("HTTP/1.1 200 OK\r\n");
  debugSerial.print("Content-Type: text/html\r\n");
  debugSerial.print("Connection: close\r\n");
  debugSerial.print("Content-Length: ");
  debugSerial.print(content.length());
  debugSerial.print("\r\n\r\n"); // End of HTTP headers
  debugSerial.print(content);

  delay(500); // Wait for sending to complete
  atSerial.println("AT+CIPCLOSE=0"); // Close the connection
}

String WebServer::generateWebPage() {
  return "<html>"
         "<body>"
         "<img src='https://wallpapercave.com/wp/wp9414303.jpg' alt='Image' />"
         "<button onclick='location.href=\"/forward\"'>Forward</button>"
         "<button onclick='location.href=\"/backward\"'>Backward</button>"
         "<button onclick='location.href=\"/left\"'>Left</button>"
         "<button onclick='location.href=\"/right\"'>Right</button>"
         "<input type='range' min='0' max='100' value='" +
         String(cm) + "' disabled>"
                        "</body>"
                        "</html>";
}

void WebServer::handleRequest(String request) {
  if (request == "GET /forward ") {
    // TODO: Implement forward movement
  } else if (request == "GET /backward ") {
    // TODO: Implement backward movement
  } else if (request == "GET /left ") {
    // TODO: Implement left movement
  } else if (request == "GET /right ") {
    // TODO: Implement right movement
  } else if (request == "GET / ") {
    sendHttpResponse(generateWebPage());
  }
}

void WebServer::handleClient() {
  while (atSerial.available()) {
    char incomingByte = atSerial.read();
    debugSerial.write(incomingByte); // Forwarding response to debug Serial

    if (incomingByte == '\n') {
      if (strstr(lineBuffer, "GET ") == lineBuffer) { // lineBuffer starts with "GET "
        debugSerial.println(lineBuffer);
        handleRequest(lineBuffer);
      }
      memset(lineBuffer, 0, sizeof(lineBuffer));
      bufferPosition = 0;
    } else if (bufferPosition < MAX_LINE_LENGTH - 1) {
      lineBuffer[bufferPosition++] = incomingByte;
    }
  }
}

bool WebServer::findAP(const char* ssid) {
  while (atSerial.available()) {
    atSerial.read();
  }
  atSerial.println("AT+CWLAP");

  bool ssidFound = false;
  bool endOfResponse = false;

  unsigned long startMillis = millis();
  unsigned long currentMillis = startMillis;

  while (!endOfResponse && (currentMillis - startMillis < 10000)) { // 5 seconds timeout
    while (atSerial.available()) {
      char incomingByte = atSerial.read();
      debugSerial.write(incomingByte); // Forwarding response to debug Serial

      if (incomingByte == '\n') {
        if (strstr(lineBuffer, ssid) != nullptr) {
          ssidFound = true;
        }
        if (strstr(lineBuffer, "OK") != nullptr || strstr(lineBuffer, "ERROR") != nullptr) {
          endOfResponse = true;
        }
        memset(lineBuffer, 0, sizeof(lineBuffer));
        bufferPosition = 0;
      } else if (bufferPosition < MAX_LINE_LENGTH - 1) {
        lineBuffer[bufferPosition++] = incomingByte;
      }
    }
    delay(10); // Short pause to allow buffer to fill up
    currentMillis = millis();
  }

  return ssidFound;
}

void WebServer::setup(const char* ssid, const char* password) {
  debugSerial.println("Setting up web server...");
  if (!findAP(ssid)) {
    debugSerial.println("The specified AP could not be found. Please check the SSID and try again.");
    return;
  }
  atSerial.println("AT+CWMODE=3");
  delay(1000);

  atSerial.print("AT+CWJAP=\"");
  atSerial.print(ssid);
  atSerial.print("\",\"");
  atSerial.print(password);
  atSerial.println("\"");
  delay(5000);

  atSerial.println("AT+CIPSERVER=1,80");
  delay(1000);

  debugSerial.println("Web server setup complete.");
}

void WebServer::handleSerialCommands() {
  while (debugSerial.available()) {
    atSerial.write(debugSerial.read()); // Forwarding command to AT Serial
  }
}

String WebServer::getLocalIP() {
  debugSerial.println("Fetching local IP...");
  atSerial.println("AT+CIPSTA?");

  unsigned long startTime = millis();
  while (millis() - startTime < 2000) {
    while (atSerial.available()) {
      char incomingByte = atSerial.read();
      debugSerial.write(incomingByte); // This line prints the raw output
    }
  }

  return "";
}