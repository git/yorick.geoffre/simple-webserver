#ifndef WebServer_h
#define WebServer_h

#include <Arduino.h>

#define MAX_LINE_LENGTH 2000

class WebServer {
private:
  Stream& atSerial;
  Stream& debugSerial;
  char lineBuffer[MAX_LINE_LENGTH];
  uint8_t bufferPosition = 0;

public:
  long cm;

  // Constructor takes in two streams for AT command and debugging (over Serial), remember to use RP2040 as the board!
  WebServer(Stream& atSerial, Stream& debugSerial);

  // Sends HTTP response with given content
  void sendHttpResponse(String content);

  // Generates web page as a string
  String generateWebPage();

  // Handles get requests to drive the car
  void handleRequest(String request);

  // Handles incoming HTTP requests
  void handleClient();

  // Checks if specified SSID is available
  bool findAP(const char* ssid);

  // Sets up the web server with given SSID and password
  void setup(const char* ssid, const char* password);

  // forwards AT commands from the debug port to the esp13
  void handleSerialCommands();

  // Fetches and returns local webserver IP address (buggy)
  String getLocalIP();
};

#endif